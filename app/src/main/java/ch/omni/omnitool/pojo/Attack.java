package ch.omni.omnitool.pojo;


public class Attack {
    private Village sendVillage;
    private Village targetVillage;
    private String unit;
    private long arrivalInMs;

    public Village getSendVillage() {
        return sendVillage;
    }

    public void setSendVillage(Village sendVillage) {
        this.sendVillage = sendVillage;
    }

    public Village getTargetVillage() {
        return targetVillage;
    }

    public void setTargetVillage(Village targetVillage) {
        this.targetVillage = targetVillage;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public long getArrivalInMs() {
        return arrivalInMs;
    }

    public void setArrivalInMs(long arrivalInMs) {
        this.arrivalInMs = arrivalInMs;
    }
}

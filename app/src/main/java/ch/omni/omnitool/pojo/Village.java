package ch.omni.omnitool.pojo;


public class Village {
    private String name;
    private int coordX;
    private int coordY;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCoordX() {
        return coordX;
    }

    public void setCoordX(int coordX) {
        this.coordX = coordX;
    }

    public int getCoordY() {
        return coordY;
    }

    public void setCoordY(int coordY) {
        this.coordY = coordY;
    }

    public String getCoord() {
        return String.valueOf(coordX) + "|" + String.valueOf(coordY);
    }
}

package ch.omni.omnitool.pojo;


import java.util.ArrayList;

public class AidRequestAttackListElement {
    private ArrayList<Attack> villageAttackList;
    private long firstAttack;
    private long lastAttack;

    public ArrayList<Attack> getVillageAttackList() {
        return villageAttackList;
    }

    public void setVillageAttackList(ArrayList<Attack> villageAttackList) {
        this.villageAttackList = villageAttackList;
    }

    public long getFirstAttack() {
        return firstAttack;
    }

    public void setFirstAttack(long firstAttack) {
        this.firstAttack = firstAttack;
    }

    public long getLastAttack() {
        return lastAttack;
    }

    public void setLastAttack(long lastAttack) {
        this.lastAttack = lastAttack;
    }
}

package ch.omni.omnitool.doer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import ch.omni.omnitool.pojo.AidRequestAttackListElement;
import ch.omni.omnitool.pojo.Attack;
import ch.omni.omnitool.pojo.Village;
import ch.omni.omnitool.util.DsUtil;
import ch.omni.omnitool.util.OmniException;
import ch.omni.omnitool.util.UnitSpeedConstants;

public class AidRequestParser {

    public static final String STARTCOORD = "[coord]";
    public static final String ENDCOORD = "[/coord]";
    public static final String STARTVILLAGE = "[b]Dorf:[/b] " + STARTCOORD;
    public static final String STARTARRIVAL = "--> Akunftszit:";
    public static final String ENDARRIVAL = "[player]";

    public static void main(String[] args) {
        String haha = "[b]Dorf:[/b] [coord]459|476[/coord]\n" +
                "[b]Wallstufe:[/b] 20\n" +
                "[b]Vrteidiger[/b]  7699  10147  0  105  0  0  0  1  1  0  0\n" +
                "\n" +
                "SKAV [coord]458|471[/coord] --> Akunftszit: 28.08.14 11:31:01:780 [player]Against All Odds[/player]\n" +
                "\n" +
                "[b]Dorf:[/b] [coord]396|471[/coord]\n" +
                "[b]Wallstufe:[/b] 20\n" +
                "[b]Vrteidiger[/b]  2455  2798  0  96  0  580  0  3  0  0  0\n" +
                "\n" +
                "Axt [coord]399|472[/coord] --> Akunftszit: 28.08.14 11:31:47:681 [player]Against All Odds[/player]\n" +
                "\n" +
                "[b]Dorf:[/b] [coord]458|470[/coord]\n" +
                "[b]Wallstufe:[/b] 20\n" +
                "[b]Vrteidiger[/b]  0  0  5802  100  2828  0  300  20  0  0  0\n" +
                "\n" +
                "Ramme [coord]456|471[/coord] --> Akunftszit: 28.08.14 11:41:35:514 [player]Against All Odds[/player]\n" +
                "Ramme [coord]456|471[/coord] --> Akunftszit: 28.08.14 11:41:35:739 [player]Against All Odds[/player]\n" +
                "Ramme [coord]456|471[/coord] --> Akunftszit: 28.08.14 11:41:36:093 [player]Against All Odds[/player]\n" +
                "Ramme [coord]456|471[/coord] --> Akunftszit: 28.08.14 11:41:38:783 [player]Against All Odds[/player]\n" +
                "\n" +
                "[b]Dorf:[/b] [coord]408|465[/coord]\n" +
                "[b]Wallstufe:[/b] 20\n" +
                "[b]Vrteidiger[/b]  0  0  6386  100  3138  0  302  2  0  0  0\n" +
                "\n" +
                "Schwärt [coord]409|468[/coord] --> Akunftszit: 28.08.14 11:44:24:026 [player]Against All Odds[/player]\n" +
                "\n" +
                "[b]Dorf:[/b] [coord]456|472[/coord]\n" +
                "[b]Wallstufe:[/b] 20\n" +
                "[b]Vrteidiger[/b]  2659  2702  0  103  0  604  0  5  0  0  0\n" +
                "\n" +
                "Ramme [coord]459|476[/coord] --> Akunftszit: 28.08.14 13:04:48:243 [player]Against All Odds[/player]\n" +
                "\n" +
                "[b]Dorf:[/b] [coord]447|457[/coord]\n" +
                "[b]Wallstufe:[/b] 20\n" +
                "[b]Vrteidiger[/b]  2035  3773  0  365  0  877  33  0  0  0  0\n" +
                "\n" +
                "AG [coord]458|471[/coord] --> Akunftszit: 28.08.14 20:58:07:778 [player]Against All Odds[/player]\n";

        AidRequestParser parser = new AidRequestParser();
        try {
            parser.parseList(haha);
        } catch (OmniException e) {
            e.printStackTrace();
        }
    }

    public static AidRequestAttackListElement parseList(String aidRequest) throws OmniException {
        ArrayList<Attack> attackList = new ArrayList<Attack>();
        long first = Long.MAX_VALUE;
        long last = -1;

        try {
            //Vorlesen am Start; Suche nach der Startposition der Zieldorfkoordinaten
            int posAcutalVillage = aidRequest.indexOf(STARTVILLAGE);

            while (posAcutalVillage != -1) {
                //Nächstes eigenes Dorf lesen, damit beim Einlesen der Angriffsliste pro Dorf nicht zu
                //weit gelesen wird.
                int posNextVillage = aidRequest.indexOf(STARTVILLAGE, posAcutalVillage + 1);

                //Endposition der Zieldorfkoordinaten finden
                int posEndCoord = aidRequest.indexOf(ENDCOORD, posAcutalVillage);

                //Zieldorfkoordinaten auslesen und speichern
                String targetCoord = aidRequest.substring(posAcutalVillage + STARTVILLAGE.length(), posEndCoord).trim();
                String[] coords = DsUtil.getCoord(targetCoord);
                Village targetVillage = new Village();
                targetVillage.setCoordX(Integer.valueOf(coords[0]));
                targetVillage.setCoordY(Integer.valueOf(coords[1]));

                //Vorlesen am Start der Angriffsliste; Liest die Startposition der ersten Angreifer-Koordinaten
                int posNextAttackStart = aidRequest.indexOf(STARTCOORD, posEndCoord);

                String naming = aidRequest.substring(posEndCoord + ENDCOORD.length(), posNextAttackStart);

                while (posNextAttackStart < posNextVillage || (posNextVillage == -1 && posNextAttackStart != -1)) {
                    //Endposition der Angreifer-Koordinaten
                    int posAttackEnd = aidRequest.indexOf(ENDCOORD, posNextAttackStart);

                    //Koordinatenstring auslesen
                    String coord = aidRequest.substring(posNextAttackStart + STARTCOORD.length(), posAttackEnd).trim();

                    //Start- und Endposition der Ankunftszeit ermitteln und auslesen
                    int posArrivalStart = aidRequest.indexOf(STARTARRIVAL, posAttackEnd);
                    int posArrivalEnds = aidRequest.indexOf(ENDARRIVAL, posArrivalStart);
                    String arrival = aidRequest.substring(posArrivalStart + STARTARRIVAL.length(), posArrivalEnds).trim();

                    //Angriffsobjekt erstellen
                    long arrivalTime = DsUtil.getDateInMs(arrival);
                    System.out.println(arrivalTime);
                    Attack attack = getAttack(coord, arrivalTime);
                    attack.setTargetVillage(targetVillage);
                    attack.setUnit(UnitSpeedConstants.getUnitFromText(naming));
                    attackList.add(attack);

                    //Den letzten Angriff bestimmen (hier originale Zeit nehmen)
                    if (last < arrivalTime) {
                        last = arrivalTime;
                    }

                    //Die laengste Mögliche LZ als erster Angriff nehmen (Längste Distanz mit AG LZ)
                    double dist = DsUtil.getDistance(attack.getTargetVillage().getCoordX(), attack.getTargetVillage().getCoordY(), attack.getSendVillage().getCoordX(), attack.getSendVillage().getCoordY());
                    arrivalTime = Math.round(arrivalTime - (dist * UnitSpeedConstants.SNOB_SPEED));
                    if (first > arrivalTime) {
                        first = arrivalTime;
                    }

                    //Naechster Angriffseintrag suchen
                    posNextAttackStart = aidRequest.indexOf(STARTCOORD, posArrivalEnds);

                    //Naechste Beschriftung suchen
                    int posPlayerEnds = aidRequest.indexOf("[/player]", posArrivalEnds);
                    if (posNextAttackStart != -1) {
                        naming = aidRequest.substring(posPlayerEnds + 9, posNextAttackStart);
                    } else {
                        naming = aidRequest.substring(posPlayerEnds + 9);
                    }
                }
                posAcutalVillage = posNextVillage;
            }

            Collections.sort(attackList, new Comparator<Attack>() {
                public int compare(Attack a1, Attack a2) {
                    return Long.valueOf(a1.getArrivalInMs()).compareTo(a2.getArrivalInMs());
                }
            });

            AidRequestAttackListElement element = new AidRequestAttackListElement();
            element.setVillageAttackList(attackList);
            element.setFirstAttack(first);
            element.setLastAttack(last);

            return element;
        } catch (Exception ex) {
            throw new OmniException("Die Untersuetzungsliste konnte nicht korrekt eingelesen werden. Bitte kopiere sie erneut und fuege sei ein.");
        }

    }

    private static Attack getAttack(String coord, long arrival) {
        Attack attack = new Attack();

        attack.setArrivalInMs(arrival);

        String[] coords = DsUtil.getCoord(coord);
        Village village = new Village();
        village.setCoordX(Integer.valueOf(coords[0]));
        village.setCoordY(Integer.valueOf(coords[1]));
        attack.setSendVillage(village);
        return attack;
    }

}

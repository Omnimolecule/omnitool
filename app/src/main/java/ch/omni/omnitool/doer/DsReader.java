package ch.omni.omnitool.doer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;

import ch.omni.omnitool.pojo.Village;

public class DsReader {
    private String url;

    public static void main(String[] args) {
//        long playerId = 1078036;
//
//        DsReader reader = new DsReader("24");
//        long id = reader.getIdForPlayerName("Against All odds");
//        ArrayList<Village> villages = reader.getVillagesForPlayerId(id);

        URL url = null;
        try {
            url = new URL("http://www.metalaz.ch/whupp/push.php?user=Against+All+Odds&inhalt=" + URLEncoder.encode("Dies ist nur ein Test", "UTF-8")+"&time=" + 1234 +"&res=reservenfeld");
            URLConnection conn = url.openConnection();
            InputStream response = conn.getInputStream();
            response.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public DsReader(String worldNo) {
        try {
            //Pruefen ob es eine Nummer ist
            Integer.valueOf(worldNo);

            url = "http://ch" + worldNo + ".staemme.ch";
        } catch (NumberFormatException e){
            e.printStackTrace();
        }

    }

    /**
     * Gibt eine Liste von Doerfern zurueck, die fuer die Spielerid gefunden wurde.
     *
     * @param playerId  Id des Spielers
     * @return  leere Liste, wenn keine Doerfer gefunden wurden.
     */
    public ArrayList<Village> getVillagesForPlayerId(long playerId){
        URL oracle = null;
        ArrayList<Village> villages = new ArrayList<Village>();
        try {
            oracle = new URL(url + "/map/village.txt");

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(oracle.openStream()));

            String inputLine;
            Village village;
            while ((inputLine = in.readLine()) != null){
                String[] inputArray = inputLine.split(",");

                if (Long.valueOf(inputArray[4]) == playerId){
                    village = new Village();
                    village.setName(URLDecoder.decode(inputArray[1], "UTF-8"));
                    village.setCoordX(Integer.valueOf(inputArray[2]));
                    village.setCoordY(Integer.valueOf(inputArray[3]));
                    villages.add(village);
                }
                System.out.println(inputLine);
            }
            in.close();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return villages;
    }

    /**
     * Versucht anhand des eingegebenen Spielernamens die ID zu ermitteln und gibt diese zurueck.
     *
     * @param name  Der korrekte Spielername
     * @return  Gibt -1 zurueck wenn keine gefunden wurde
     */
    public long getIdForPlayerName(String name){
        URL oracle = null;
        long playerId;
        try {
            oracle = new URL(url + "/map/player.txt");

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(oracle.openStream()));

            String inputLine;
            Village village;
            String urlName = URLEncoder.encode(name, "UTF-8");
            while ((inputLine = in.readLine()) != null){
                String[] inputArray = inputLine.split(",");
                if (inputArray[1].toUpperCase().equals(urlName.toUpperCase())){
                    playerId = Long.valueOf(inputArray[0]);
                    in.close();
                    return playerId;
                }

            }
            in.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return -1;
    }
}

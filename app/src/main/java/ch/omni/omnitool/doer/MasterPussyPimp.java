package ch.omni.omnitool.doer;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import ch.omni.omnitool.database.AttackList;
import ch.omni.omnitool.database.DbHelper;
import ch.omni.omnitool.pojo.AidRequestAttackListElement;
import ch.omni.omnitool.pojo.Attack;
import ch.omni.omnitool.pojo.CompareElement;
import ch.omni.omnitool.pojo.Village;
import ch.omni.omnitool.util.DsUtil;
import ch.omni.omnitool.util.OmniException;
import ch.omni.omnitool.util.UnitSpeedConstants;

public class MasterPussyPimp {
    private static final int TOLERANZWERT = 3500;
    private boolean useAttackLabel = false;
    private AidRequestAttackListElement aidRequestAttackListElement;
    private ArrayList<Long> attackSendTimeDbList = new ArrayList<Long>();
    ArrayList<Long> usedOnce = new ArrayList<Long>();

    /**
     * Vergleicht die Angriffe aus den Notification mit den Angriffen aus der Unterstuetzungsanfrage.
     * Zuhilfe nimmt er dabei die Einheitengeschwindigkeit sowie die Distanz zwischen Angreifer- und
     * Zieldorf. Es wird von den Angriffen aus den Notification ausgegangen.
     *
     * @param aidRequestAttackListElement Auswertung aus der Unterstuetzungsanfrage
     * @param useAttackLabel    True = Die ausgelesenen Einheiten aus den Angriffsbeschriftungen werden priorisiert verwendet bei der Berechnung.
     * @param context         Activity um die Daten aus der DB zu holen
     * @return ErgebnisMap mit den Zieldoerfern als Key und den Angriffen nach
     * Ankunftszeit sortiert.
     */
    public HashMap<Village, ArrayList<Attack>> compare(AidRequestAttackListElement aidRequestAttackListElement, boolean useAttackLabel, Context context) throws OmniException {
        this.aidRequestAttackListElement = aidRequestAttackListElement;
        this.useAttackLabel = useAttackLabel;
        HashMap<Attack, ArrayList<CompareElement>> attackWithPossibleSendTimeMap = new HashMap<Attack, ArrayList<CompareElement>>();

        readAttackListFromDb(context);

        //Herausfinden aller möglichen Kombinationen
        getAllPossibilities(aidRequestAttackListElement, attackWithPossibleSendTimeMap);

        //Sortieren und zuweisen der Ergebnisse
        HashMap<Village, ArrayList<Attack>> sortedResultMap = getSortedResult(attackWithPossibleSendTimeMap);

        return sortedResultMap;

    }

    /**
     * Vergleicht zu jedem Angriff aus der Liste alle moeglichen Kombinationen (EinheitenLZ * Distanz) und prueft
     * ob eine auf 3Sek passende Abschickzeit vorhanden ist. Alle passenden Zeiten werden zu diesem Angriff
     * gespeichert. Wenn die Angriffsbeschriftungen gebraucht werden dürfen, wird zuerst nach diesen sortiert.
     *
     * @param aidRequestAttackListElement   Die einzelnen Angriffselemente
     * @param compareMap        Die Map, in welche alles gespeichert wird
     * @throws OmniException    Wirft eine OmniException wenn keine passenden Angriffe in der Datenbank gespeichert sind
     */
    private void getAllPossibilities(AidRequestAttackListElement aidRequestAttackListElement, HashMap<Attack, ArrayList<CompareElement>> compareMap) throws OmniException {
        ArrayList<Long> used2Time = new ArrayList<Long>();
        if (!attackSendTimeDbList.isEmpty()) {
            for (Attack aidRequestAttack : aidRequestAttackListElement.getVillageAttackList()) {

                //Wenn mit den Label gearbeitet wird, schauen ob ein label passt.
                if (useAttackLabel) {
                    long unitSpeed = UnitSpeedConstants.getSpeedFromUnit(aidRequestAttack.getUnit());
                    if (unitSpeed != 0) {
                        getPossibilitiesFromEstimatedTime(compareMap, used2Time, aidRequestAttack, unitSpeed);
                    }
                }
                if (!useAttackLabel || !compareMap.containsKey(aidRequestAttack)) {
                    //Zuerst vom Adelsgeschlecht ausgehen
                    long unitSpeed = UnitSpeedConstants.SNOB_SPEED;

                    //So lange die Zeiten ueberpruefen, bis alle Einheiten durch sind.
                    while (unitSpeed != -1) {
                        getPossibilitiesFromEstimatedTime(compareMap, used2Time, aidRequestAttack, unitSpeed);

                        unitSpeed = UnitSpeedConstants.getNextUnit(unitSpeed);
                    }
                }
            }
        } else {
            throw new OmniException("Es wurden keine passenden Angriffsnotification gefunden. Ist die Unterstuetzungsanfrage korrekt?");
        }
    }

    /**
     * Vergleicht die Laufzeiten einer Einheit eines bestimmten Angriffes mit allen möglichen Abschickzeiten und
     * schreibt sie in die compareMap
     *
     * @param compareMap    Map, in welche die gefundenen Angriff-Notification Kombinationen gespeichert werden
     * @param used2Time     Liste in der alle Notifications stehen, die mehr als einmal als Abschickzeit möglich wären
     * @param aidRequestAttack  Das Unterstützungsangriff objekt
     * @param unitSpeed Die Geschwindigkeit der gefragten Einheit
     */
    private void getPossibilitiesFromEstimatedTime(HashMap<Attack, ArrayList<CompareElement>> compareMap, ArrayList<Long> used2Time, Attack aidRequestAttack, long unitSpeed) {
        double estimatedTime = getEstimatedTime(aidRequestAttack, unitSpeed);

        for (Long sendTime : attackSendTimeDbList) {
            if (estimatedTime + TOLERANZWERT >= sendTime && estimatedTime - TOLERANZWERT <= sendTime) {
                CompareElement compareElement = new CompareElement();
                compareElement.setSendTime(sendTime);
                compareElement.setUnit(UnitSpeedConstants.getUnitFromSpeed(unitSpeed));
                compareElement.setDifference(sendTime - estimatedTime);

                addCompareElementToCompareMap(compareMap, aidRequestAttack, compareElement);
                if (!usedOnce.contains(sendTime) && !used2Time.contains(sendTime)) {
                    usedOnce.add(sendTime);
                } else {
                    used2Time.add(sendTime);
                    usedOnce.remove(sendTime);
                }
            }
        }
    }

    /**
     * Berechnet die Dauer, die eine gewisse Einheit vom Angreifer zum Verteidigerdorf hat und gibt sie zurueck.
     *
     * @param aidRequestAttack Der Angriff aus der Unterstuetzungsanfrage
     * @param unitSpeed        Die Geschwindigkeit der gefragten Einheit
     * @return ungefähre Laufzeit als Kommazahl (ist genauer)
     */
    private double getEstimatedTime(Attack aidRequestAttack, long unitSpeed) {
        Village sendVillage = aidRequestAttack.getSendVillage();
        Village targetVillage = aidRequestAttack.getTargetVillage();

        double dist = DsUtil.getDistance(sendVillage.getCoordX(), sendVillage.getCoordY()
                , targetVillage.getCoordX(), targetVillage.getCoordY());

        //Berechnet die geschaetze Abschickzeit anhand der verwendeten Einheit
        return aidRequestAttack.getArrivalInMs() - (dist * unitSpeed);
    }

    /**
     * Prueft welche AngriffsElemente nur eine Moegliche Abschickzeit haben und legt diese dann als richtig fest.
     * Diese Zeit wird aus der Liste entfernt. Alle AngriffsElemente werden geprüft und Angriffsmöglichkeiten, die
     * nicht mehr in der Liste sind, werden entfernt. Dann beginnt es wieder von vorne, bis jeder Angriff nur noch
     * eine zugewiesene Abschickzeit hat.
     *
     * @param compareMap Map mit den Angriffsobjekten und den Listen der moeglichen Angriffe
     * @return ErgebnisMap mit den zugewiesenen Angriffen
     */
    private HashMap<Village, ArrayList<Attack>> getSortedResult(HashMap<Attack, ArrayList<CompareElement>> compareMap) {
        HashMap<Village, ArrayList<Attack>> ergebnisse = new HashMap<Village, ArrayList<Attack>>();
        boolean changed = true;

        //Wenn ein Angriffsobjekt nicht mehr als einen Angriff hat, ist klar, dass der Angriff definitiv
        //stimmen muss und kann fix zugeteilt werden.
        while (!attackSendTimeDbList.isEmpty() && !compareMap.isEmpty() && changed) {

            changed = false;

            for (Attack aidRequestAttack : compareMap.keySet()) {
                ArrayList<CompareElement> tempListe = compareMap.get(aidRequestAttack);
                if (tempListe.size() == 1) {
                    Attack effectiveAttack = new Attack();
                    effectiveAttack.setArrivalInMs(aidRequestAttack.getArrivalInMs());
                    effectiveAttack.setSendVillage(aidRequestAttack.getSendVillage());
                    effectiveAttack.setUnit(tempListe.get(0).getUnit());

                    addResultToErgebnisse(ergebnisse, aidRequestAttack.getTargetVillage(), effectiveAttack);
                    attackSendTimeDbList.remove(tempListe.get(0).getSendTime());
                    changed = true;
                } else {
                    for (CompareElement element : tempListe) {
                        //Wenn ein Angriff zwar mehrere Optionen hat, eine dieser Optionen aber nur für diesen
                        //einen Angriff zur Verfügung steht, dann kann diese Option auch fix zugeteilt werden.
                        if (usedOnce.contains(element.getSendTime())) {
                            Attack effectiveAttack = new Attack();
                            effectiveAttack.setArrivalInMs(aidRequestAttack.getArrivalInMs());
                            effectiveAttack.setSendVillage(aidRequestAttack.getSendVillage());
                            effectiveAttack.setUnit(tempListe.get(0).getUnit());

                            addResultToErgebnisse(ergebnisse, aidRequestAttack.getTargetVillage(), effectiveAttack);
                            attackSendTimeDbList.remove(element.getSendTime());
                            changed = true;
                        }
                    }
                }
            }

            //Prüfen ob man einen Eintrag loeschen kann, weil die Abschickzeit schon an einen anderen
            //Angriff vergeben wurde oder ob er schon zugeteilt ist.
            //Nebenbei notieren ob alle Angriffe die gleiche Einheit darstellen
            for (Attack aidRequestAttack : compareMap.keySet()) {
                ArrayList<CompareElement> tempListe = compareMap.get(aidRequestAttack);
                ArrayList<CompareElement> newList = new ArrayList<CompareElement>();
                if (!tempListe.isEmpty()) {
                    String unit = "";
                    boolean allSame = true;
                    for (CompareElement element : tempListe) {
                        if (attackSendTimeDbList.contains(Long.valueOf(element.getSendTime()))) {
                            newList.add(element);

                            //Herausfinden ob alle Angriffe die gleiche Einheit haben, denn dann ist
                            //egal welcher es genau ist
                            if (unit.isEmpty()) {
                                unit = element.getUnit();
                            } else if (!unit.equals(element.getUnit())) {
                                allSame = false;
                            }
                        } else {
                            changed = true;
                            allSame = false;
                        }
                    }

                    //Wenn alle möglichen Angriffe die gleiche Einheit haben, dann kann die Einheit
                    //uebernommen werden, auch wenn wir die genaue Abschickzeit nicht kennen.
                    if (allSame) {
                        Attack effectiveAttack = new Attack();
                        effectiveAttack.setArrivalInMs(aidRequestAttack.getArrivalInMs());
                        effectiveAttack.setSendVillage(aidRequestAttack.getSendVillage());
                        effectiveAttack.setUnit(tempListe.get(0).getUnit());

                        addResultToErgebnisse(ergebnisse, aidRequestAttack.getTargetVillage(), effectiveAttack);
                        //Eine Leere Liste um zu zeigen, dass dieser "Fall" abgeschlossen sit.
                        newList = new ArrayList<CompareElement>();
                    }

                    compareMap.put(aidRequestAttack, newList);
                }
            }
        }
        for (Attack aidRequestAttack : compareMap.keySet()) {
            System.out.println("Zieldörfli: " + aidRequestAttack.getTargetVillage().getCoordX() + "|" + aidRequestAttack.getTargetVillage().getCoordY());
            for (CompareElement element : compareMap.get(aidRequestAttack)) {
                System.out.println("Möglichkeit " + element.getUnit() + " " + element.getSendTime());
            }
        }
        return ergebnisse;
    }

    /**
     * Fuegt ein Element in die Arraylist des betroffenen Mapeintrages hinzu
     *
     * @param attackWithPossibleSendTimeMap       Map in die das Element eingefügt wird
     * @param aidRequestAttack Key
     * @param compareElement   Value
     */
    private void addCompareElementToCompareMap(HashMap<Attack, ArrayList<CompareElement>> attackWithPossibleSendTimeMap, Attack aidRequestAttack, CompareElement compareElement) {
        ArrayList<CompareElement> attackList;

        if (attackWithPossibleSendTimeMap.containsKey(aidRequestAttack)) {
            attackList = attackWithPossibleSendTimeMap.get(aidRequestAttack);
        } else {
            attackList = new ArrayList<CompareElement>();
        }

        attackList.add(compareElement);
        attackWithPossibleSendTimeMap.put(aidRequestAttack, attackList);
    }

    /**
     * Fügt den herausgefundenen Angriff in die Map ein und sortiert die Angriffe des betroffenen
     * Dorfes danach nach Abschickzeit.
     *
     * @param ergebnisse    Map, in die der Angriff gefüllt wird
     * @param targetVillage ZielDorf, dient als Key
     * @param resultAttack  Angriff der eingefügt wird
     */
    private void addResultToErgebnisse(HashMap<Village, ArrayList<Attack>> ergebnisse, Village targetVillage, Attack resultAttack) {
        ArrayList<Attack> attackList;

        if (ergebnisse.containsKey(targetVillage)) {
            attackList = ergebnisse.get(targetVillage);
        } else {
            attackList = new ArrayList<Attack>();
        }

        attackList.add(resultAttack);
        Collections.sort(attackList, new Comparator<Attack>() {
            public int compare(Attack a1, Attack a2) {
                return Long.valueOf(a1.getArrivalInMs()).compareTo(a2.getArrivalInMs());
            }
        });
        ergebnisse.put(targetVillage, attackList);
    }

    /**
     * Liest die Angriffsnotificationliste aus der Datenbank und speichert sie ab.
     *
     * @param context Kontext der Activity
     */
    private void readAttackListFromDb(Context context) {
        DbHelper adbHelper = new DbHelper(context);
        SQLiteDatabase db = adbHelper.getWritableDatabase();
        String[] projection = {AttackList.AttackEntry.COLUMN_NAME_TARGET_NAME, AttackList.AttackEntry.COLUMN_NAME_SEND_TIME};
        String selection = AttackList.AttackEntry.COLUMN_NAME_SEND_TIME + " <= " + aidRequestAttackListElement.getLastAttack()
                + " AND " + AttackList.AttackEntry.COLUMN_NAME_SEND_TIME + " >= " + aidRequestAttackListElement.getFirstAttack();
        String order = AttackList.AttackEntry.COLUMN_NAME_SEND_TIME + " ASC";

        Cursor c = db.query(AttackList.AttackEntry.TABLE_NAME, projection, selection, null, null, null, order);


        if (c.getCount() > 0) {
            c.moveToFirst();
            //String a = c.getString(c.getColumnIndex(AttackList.AttackEntry.COLUMN_NAME_TARGET_NAME));
            Long time = c.getLong(c.getColumnIndex(AttackList.AttackEntry.COLUMN_NAME_SEND_TIME));
            attackSendTimeDbList.add(time);

            if (c.getCount() > 1) {
                while (c.moveToNext()) {
                    time = c.getLong(c.getColumnIndex(AttackList.AttackEntry.COLUMN_NAME_SEND_TIME));
                    attackSendTimeDbList.add(time);
                }
            }
        }
    }


    /**
     * Diese Methode ist nur zum testen der Funktionalitaet von MasterPussyPimp. Sie ruft nicht die Datenbank auf, sondern nimmt eine Liste
     * von Notifications entgegen.
     *
     * @param aidRequestAttackListElement Auswertung aus der Unterstuetzungsanfrage
     * @param liste                       Liste der generierten Notifications
     * @param useAttackLabel              True = Die ausgelesenen Einheiten aus den Angriffsbeschriftungen werden priorisiert verwendet bei der Berechnung.
     * @return ErgebnisMap mit den Zieldoerfern als Key und den Angriffen nach
     * Ankunftszeit sortiert.
     * @throws OmniException
     */
    public HashMap<Village, ArrayList<Attack>> compareForTest(AidRequestAttackListElement aidRequestAttackListElement, ArrayList<Long> liste, boolean useAttackLabel) throws OmniException {
        this.aidRequestAttackListElement = aidRequestAttackListElement;
        this.attackSendTimeDbList = liste;
        this.useAttackLabel = useAttackLabel;
        HashMap<Attack, ArrayList<CompareElement>> compareMap = new HashMap<Attack, ArrayList<CompareElement>>();

        //Herausfinden aller möglichen Kombinationen
        getAllPossibilities(aidRequestAttackListElement, compareMap);

        //Sortieren und zuweisen der Ergebnisse
        return getSortedResult(compareMap);

    }
}

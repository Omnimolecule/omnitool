package ch.omni.omnitool.doer;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import ch.omni.omnitool.pojo.AidRequestAttackListElement;
import ch.omni.omnitool.pojo.Attack;
import ch.omni.omnitool.pojo.Village;
import ch.omni.omnitool.util.OmniException;

public class GenerateTestData {
    public static void main(String[] args) {
        getDBLongs();

        String blub = "[b]Dorf:[/b] [coord]518|534[/coord]\n" +
                "\n" +
                "[b]Wallstufe:[/b] 20\n" +
                "[b]Vrteidiger[/b]  7699  10147  0  105  0  0  0  1  1  0  0\n" +
                "\n" +
                "Lkav [coord]524|534[/coord] --> Akunftszit: 23.10.14 10:52:02:586 [player]Against All Odds[/player]\n" +
                "\n" +
                "Schwert [coord]526|531[/coord] --> Akunftszit: 23.10.14 13:00:00:125[player]Against All Odds[/player]\n" +
                "\n" +
                "\n" +
                "\n" +
                "[b]Dorf:[/b] [coord]524|534[/coord]\n" +
                "\n" +
                "[b]Wallstufe:[/b] 20\n" +
                "[b]Vrteidiger[/b]  7699  10147  0  105  0  0  0  1  1  0  0\n" +
                "\n" +
                "Ramme [coord]518|529[/coord] --> Akunftszit: 23.10.14 13:46:21:978 [player]Against All Odds[/player]\n" +
                "\n" +
                "Schwert [coord]522|531[/coord] --> Akunftszit: 23.10.14 18:44:47:175 [player]Against All Odds[/player]\n" +
                "\n" +
                "Schwert [coord]526|531[/coord] --> Akunftszit: 23.10.14 18:44:47:598  [player]Against All Odds[/player]\n" +
                "\n" +
                "\n" +
                "\n" +
                "[b]Dorf:[/b] [coord]519|533[/coord]\n" +
                "\n" +
                "[b]Wallstufe:[/b] 20\n" +
                "[b]Vrteidiger[/b]  7699  10147  0  105  0  0  0  1  1  0  0\n" +
                "\n" +
                "Ramme [coord]518|534[/coord] --> Akunftszit: 23.10.14 15:36:05:687 [player]Against All Odds[/player]\n" +
                "\n" +
                "Spy [coord]522|528[/coord] --> Akunftszit: 23.10.14 17:25:35:987 [player]Against All Odds[/player]\n" +
                "\n" +
                "\n" +
                "\n" +
                "[b]Dorf:[/b] [coord]522|531[/coord]\n" +
                "\n" +
                "[b]Wallstufe:[/b] 20\n" +
                "[b]Vrteidiger[/b]  7699  10147  0  105  0  0  0  1  1  0  0\n" +
                "\n" +
                "Ramme [coord]522|528[/coord] --> Akunftszit: 23.10.14 09:00:00:038[player]Against All Odds[/player]\n" +
                "\n" +
                "AG [coord]524|534[/coord] --> Akunftszit: 23.10.14 09:00:00:622 [player]Against All Odds[/player]\n" +
                "\n" +
                "AG [coord]524|534[/coord] --> Akunftszit: 23.10.14 09:00:00:722 [player]Against All Odds[/player]\n" +
                "\n" +
                "AG [coord]524|534[/coord] --> Akunftszit: 23.10.14 09:00:00:822 [player]Against All Odds[/player]\n" +
                "\n" +
                "AG [coord]524|534[/coord] --> Akunftszit: 23.10.14 09:00:00:922 [player]Against All Odds[/player]\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "[b]Dorf:[/b] [coord]526|531[/coord]\n" +
                "\n" +
                "[b]Wallstufe:[/b] 20\n" +
                "[b]Vrteidiger[/b]  7699  10147  0  105  0  0  0  1  1  0  0\n" +
                "\n" +
                "Schwert [coord]518|534[/coord] --> Akunftszit: 23.10.14 14:56:13:964 [player]Against All Odds[/player]\n" +
                "\n" +
                "\n" +
                "\n" +
                "[b]Dorf:[/b] [coord]517|529[/coord]\n" +
                "\n" +
                "[b]Wallstufe:[/b] 20\n" +
                "[b]Vrteidiger[/b]  7699  10147  0  105  0  0  0  1  1  0  0\n" +
                "\n" +
                "Axt [coord]518|534[/coord] --> Akunftszit: 23.10.14 08:15:46:113 [player]Against All Odds[/player]\n" +
                "\n" +
                "Ramme [coord]519|533[/coord] --> Akunftszit: 23.10.14 09:49:00:397 [player]Against All Odds[/player]\n" +
                "\n" +
                "Spy [coord]522|531[/coord] --> Akunftszit: 23.10.14 14:51:16:547 [player]Against All Odds[/player]\n" +
                "\n" +
                "Skav [coord]526|531[/coord] --> Akunftszit: 23.10.14 15:15:15:151[player]Against All Odds[/player]\n" +
                "\n" +
                "\n" +
                "\n" +
                "[b]Dorf:[/b] [coord]518|529[/coord]\n" +
                "\n" +
                "[b]Wallstufe:[/b] 20\n" +
                "[b]Vrteidiger[/b]  7699  10147  0  105  0  0  0  1  1  0  0\n" +
                "\n" +
                "Ramme [coord]522|531[/coord] --> Akunftszit: 23.10.14 13:37:15:675 [player]Against All Odds[/player]\n" +
                "\n" +
                "Axt [coord]517|529[/coord] --> Akunftszit: 23.10.14 15:27:31:378 [player]Against All Odds[/player]\n" +
                "\n" +
                "\n" +
                "\n" +
                "[b]Dorf:[/b] [coord]522|528[/coord]\n" +
                "\n" +
                "[b]Wallstufe:[/b] 20\n" +
                "[b]Vrteidiger[/b]  7699  10147  0  105  0  0  0  1  1  0  0\n" +
                "\n" +
                "Axt [coord]525|527[/coord] --> Akunftszit: 23.10.14 19:46:48:486 [player]Against All Odds[/player]\n" +
                "\n" +
                "\n" +
                "\n" +
                "[b]Dorf:[/b] [coord]525|527[/coord]\n" +
                "\n" +
                "[b]Wallstufe:[/b] 20\n" +
                "[b]Vrteidiger[/b]  7699  10147  0  105  0  0  0  1  1  0  0\n" +
                "\n" +
                "Spy [coord]524|534[/coord] --> Akunftszit: 23.10.14 14:45:59:123 [player]Against All Odds[/player]\n" +
                "\n" +
                "Skav [coord]518|529[/coord] --> Akunftszit: 23.10.14 16:13:45:578 [player]Against All Odds[/player]\n" +
                "\n" +
                "\n" +
                "\n" +
                "[b]Dorf:[/b] [coord]524|534[/coord]\n" +
                "\n" +
                "[b]Wallstufe:[/b] 20\n" +
                "[b]Vrteidiger[/b]  7699  10147  0  105  0  0  0  1  1  0  0\n" +
                "\n" +
                "Ramme [coord]524|527[/coord] --> Akunftszit: 23.10.14 08:00:00:345 [player]Against All Odds[/player]\n" +
                "\n" +
                "AG [coord]518|534[/coord] --> Akunftszit: 23.10.14 08:00:00:548 [player]Against All Odds[/player]";

        AidRequestAttackListElement element = new AidRequestAttackListElement();
        try {
            element = AidRequestParser.parseList(blub);
        } catch (OmniException e) {
            e.printStackTrace();
        }

        ArrayList<Long> dbListe = new ArrayList<Long>();
        dbListe.add(1414050722586L);
        dbListe.add(1414050722125L);
        dbListe.add(1414050722978L);
        dbListe.add(1414077927847L);
        dbListe.add(1414077928270L);
        dbListe.add(1414068820102L);
        dbListe.add(1414074787272L);
        dbListe.add(1414042200038L);
        dbListe.add(1414040029622L);
        dbListe.add(1414040029722L);
        dbListe.add(1414040029822L);
        dbListe.add(1414040029922L);
        dbListe.add(1414057695964L);
        dbListe.add(1414039440113L);
        dbListe.add(1414042491397L);
        dbListe.add(1414065769547L);
        dbListe.add(1414064031151L);
        dbListe.add(1414056186675L);
        dbListe.add(1414069771378L);
        dbListe.add(1414082993486L);
        dbListe.add(1414064541123L);
        dbListe.add(1414068821578L);
        dbListe.add(1414031400345L);
        dbListe.add(1414031400548L);

        MasterPussyPimp mpp = new MasterPussyPimp();
        try {
            HashMap<Village, ArrayList<Attack>> result = mpp.compareForTest(element, dbListe, true);

            for (Village village : result.keySet()) {
                System.out.println("Zieldorf: " + village.getCoordX() + "|" + village.getCoordY());
                ArrayList<Attack> atts = result.get(village);
                System.out.println("Angriffe:");
                for (Attack att : atts) {
                    System.out.println(att.getUnit() + " - " + att.getSendVillage().getCoord() + " - " + new Date(att.getArrivalInMs()));
                }
            }
        } catch (OmniException e) {
            e.printStackTrace();
        }
    }

    public static void getDBLongs() {
        String[] liste = new String[]{
                "1414050722586",
                "1414050722125",
                "1414050722978",
                "1414077927847",
                "1414077928270",
                "1414068820102",
                "1414074787272",
                "1414042200038",
                "1414040029622",
                "1414040029722",
                "1414040029822",
                "1414040029922",
                "1414057695964",
                "1414039440113",
                "1414042491397",
                "1414065769547",
                "1414064031151",
                "1414056186675",
                "1414069771378",
                "1414082993486",
                "1414064541123",
                "1414068821578",
                "1414031400345",
                "1414031400548"
        };


        for (String s : liste) {

            System.out.println("dbListe.add(" + s + "L);");
        }
    }
}

package ch.omni.omnitool.service;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import ch.omni.omnitool.R;
import ch.omni.omnitool.database.AttackList;
import ch.omni.omnitool.database.DbHelper;
import ch.omni.omnitool.util.SntpClient;

public class AttackListener extends NotificationListenerService {
    private static final String PACKAGE_NAME_INNO = "air.com.innogames.staemme";
    private static final String SEARCH_STRING = "Unfründlechi Truppe";
    private static final String URL = "http://www.metalaz.ch/whupp/push.php?user=%s&inhalt=%s&time=%s&res=";

    private DbHelper dbHelper;
    private SQLiteDatabase db;
    private SntpClient sntpClient;

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        if (sbn.getPackageName().equals(PACKAGE_NAME_INNO) && sbn.getNotification().tickerText.toString().contains(SEARCH_STRING)) {
            System.out.println("onNotificationPosted");
            System.out.println("ID :" + sbn.getId() + "\t" + sbn.getNotification().tickerText + "\t" + sbn.getPackageName());
            System.out.println(sbn.getPostTime());
            System.out.println(sbn.getNotification().when);

            new WriteToDatabase().execute(sbn);
        }

    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        dbHelper = new DbHelper(getApplicationContext());
        db = dbHelper.getWritableDatabase();
        sntpClient = new SntpClient();
    }

    class WriteToDatabase extends AsyncTask<StatusBarNotification, Long, Long> {

        @Override
        protected Long doInBackground(StatusBarNotification... statusBarNotifications) {
            StatusBarNotification sbn = statusBarNotifications[0];

            //Wenn die Zeit das letzte Mal vor etwa 5 min abgefragt wurde, wird sie neu geholt, sonst
            //verwenden wir noch die alte Referenz
            if ((sbn.getPostTime() - 300000) > sntpClient.getNtpTime()) {
                if (!sntpClient.requestTime("ch.pool.ntp.org", 10000)) {
                    System.out.println("Fehler!!");
                } else {
                    System.out.println("Zeit neu geholt!");
                }
            }

            long sendTime;
            if (sntpClient != null) {
                sendTime = sntpClient.getNtpTime() + SystemClock.elapsedRealtime() - sntpClient.getNtpTimeReference();
            } else {
                sendTime = sbn.getPostTime();
            }

            System.out.println("Unterschied: " + (sendTime - sbn.getPostTime()));

            URL url = null;
            try {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.sharedPrefs), Context.MODE_PRIVATE);
                String savedNick = sharedPref.getString(getString(R.string.nickname), "");

                String urlString = String.format(URL, URLEncoder.encode(savedNick, "UTF-8"), URLEncoder.encode(String.valueOf(sbn.getNotification().tickerText), "UTF-8"), sendTime);
                url = new URL(urlString);
                URLConnection conn = url.openConnection();
                InputStream response = conn.getInputStream();
                response.close();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            ContentValues values = new ContentValues();
            values.put(AttackList.AttackEntry.COLUMN_NAME_TARGET_NAME, (String) sbn.getNotification().tickerText);
            values.put(AttackList.AttackEntry.COLUMN_NAME_SEND_TIME, sendTime);

            db.insert(AttackList.AttackEntry.TABLE_NAME,
                    AttackList.AttackEntry.COLUMN_NAME_SEND_TIME, values);


            return null;
        }
    }
}

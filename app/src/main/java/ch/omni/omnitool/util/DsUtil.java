package ch.omni.omnitool.util;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ch.omni.omnitool.pojo.Village;

public class DsUtil {

    /**
     * Gibt die einzelnen Koordinaten aus einem KoordinatenString zurück
     *
     * @param coords Input in folgendem Format nnn|nnn (ohne ())
     * @return Index 0 = x; Index 1 = y
     */
    public static String[] getCoord(String coords) {
        int pos = coords.indexOf("|");
        String x = coords.substring(0, pos);
        String y = coords.substring(pos + 1, coords.length());

        return new String[]{x, y};
    }

    /**
     * Parst ein Datum aus einem Datumsstring und gibt es als millisekunden zurück
     *
     * @param date Datumsstring(Format: dd.MM.yy hh:mm:ss:SSS)
     * @return -1 wenn das Datum nicht geparst werden konnte
     */
    public static long getDateInMs(String date) {
        DateFormat formatter = new SimpleDateFormat("dd.MM.yy HH:mm:ss:SSS");
        try {
            return formatter.parse(date).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * Berechnet die Distanz zwischen zwei Koordinaten
     *
     * @param x1 X vom Dorf 1
     * @param y1 Y vom Dorf 1
     * @param x2 X vom Dorf 2
     * @param y2 Y vom Dorf 2
     * @return Distanz als Kommazahl
     */
    public static double getDistance(int x1, int y1, int x2, int y2) {
        return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

    /**
     * Klasse um mal nachzupruefen was die "offizielle" Abschickzeit eines Angriffs war
     *
     * @param attacker  Dorf das angreift
     * @param target    Angegriffenes dorf
     * @param time      Ankunftszeit
     * @param unitspeed Einheit (bei eigenen Angriffen weiss man das ;))
     * @return Datumstring
     */
    public static String getSendTimeFromAllInformation(Village attacker, Village target, String time, long unitspeed) {
        double dist = DsUtil.getDistance(attacker.getCoordX(), attacker.getCoordY()
                , target.getCoordX(), target.getCoordY());

        long arrivalTime = getDateInMs(time);

        //Berechnet die geschaetze Abschickzeit anhand der verwendeten Einheit
        //String Guesel, damit die hintersten 3 Stellen durch Nullen ersetzt werden (bei der berechnung
        //werden von DS keine Milisekunden einbezogen)
        String runtime = String.valueOf(Double.valueOf((dist * unitspeed)).longValue());
        runtime = runtime.substring(0, runtime.length() - 3) + "000";
        long estimatedTime = arrivalTime - Long.valueOf(runtime);

        DateFormat dtfm = new SimpleDateFormat("dd.MM.yy hh:mm:ss:SSS");

        return dtfm.format(new Date(estimatedTime));
    }

}

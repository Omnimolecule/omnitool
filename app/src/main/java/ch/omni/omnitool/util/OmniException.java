package ch.omni.omnitool.util;


public class OmniException extends Exception {

    public OmniException() {
    }

    public OmniException(String detailMessage) {
        super(detailMessage);
    }

    public OmniException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public OmniException(Throwable throwable) {
        super(throwable);
    }
}

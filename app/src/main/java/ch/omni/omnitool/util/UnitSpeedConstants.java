package ch.omni.omnitool.util;


public class UnitSpeedConstants {
    public static final String SPY = "Spaeher";
    public static final int SPY_SPEED = 540000;
    public static final String LIGHT = "Lkav";
    public static final int LIGHT_SPEED = 600000;
    public static final String HEAVY = "Skav";
    public static final int HEAVY_SPEED = 660000;
    public static final String AXE_SPEAR = "Axt/Speer";
    public static final int AXE_SPEAR_SPEED = 1080000;
    public static final String SWORD = "Schwert";
    public static final int SWORD_SPEED = 1320000;
    public static final String RAM_CATA = "Ramme/Kata";
    public static final int RAM_CATA_SPEED = 1800000;
    public static final String SNOB = "AG";
    public static final int SNOB_SPEED = 2100000;

    /**
     * Gibt den Namen der Einheit anhand ihrer Geschwindigkeit zurück
     *
     * @param speed Einheitgeschwindigkeit
     * @return Name der Einheit
     */
    public static String getUnitFromSpeed(long speed) {
        if (speed == SPY_SPEED) {
            return SPY;
        } else if (speed == LIGHT_SPEED) {
            return LIGHT;
        } else if (speed == HEAVY_SPEED) {
            return HEAVY;
        } else if (speed == AXE_SPEAR_SPEED) {
            return AXE_SPEAR;
        } else if (speed == SWORD_SPEED) {
            return SWORD;
        } else if (speed == RAM_CATA_SPEED) {
            return RAM_CATA;
        } else if (speed == SNOB_SPEED) {
            return SNOB;
        }

        return "";

    }

    /**
     * Gibt die Einheitengeschwindigkeiten absteigend zurück
     *
     * @param lastTime Zuletzte verwendete Einheitengeschwindigkeit
     * @return nächste Einheitengeschwindigkeit; -1 Wenn die letzet Einheit erreicht wurde
     */
    public static long getNextUnit(long lastTime) {
        if (lastTime == UnitSpeedConstants.SNOB_SPEED) {
            return UnitSpeedConstants.RAM_CATA_SPEED;
        }
        if (lastTime == UnitSpeedConstants.RAM_CATA_SPEED) {
            return UnitSpeedConstants.SWORD_SPEED;
        }
        if (lastTime == UnitSpeedConstants.SWORD_SPEED) {
            return UnitSpeedConstants.AXE_SPEAR_SPEED;
        }
        if (lastTime == UnitSpeedConstants.AXE_SPEAR_SPEED) {
            return UnitSpeedConstants.HEAVY_SPEED;
        }
        if (lastTime == UnitSpeedConstants.HEAVY_SPEED) {
            return UnitSpeedConstants.LIGHT_SPEED;
        }
        if (lastTime == UnitSpeedConstants.LIGHT_SPEED) {
            return UnitSpeedConstants.SPY_SPEED;
        }

        return -1;

    }

    /**
     * Gibt die Geschwindigkeit der Einheit anhand von ihrem Namen zurueck.
     *
     * @param unit Einheitenname
     * @return Geschwindgkeit der Einheit
     */
    public static long getSpeedFromUnit(String unit) {
        if (unit.equals(UnitSpeedConstants.SNOB)) return UnitSpeedConstants.SNOB_SPEED;
        if (unit.equals(UnitSpeedConstants.RAM_CATA)) return UnitSpeedConstants.RAM_CATA_SPEED;
        if (unit.equals(UnitSpeedConstants.SWORD)) return UnitSpeedConstants.SWORD_SPEED;
        if (unit.equals(UnitSpeedConstants.AXE_SPEAR)) return UnitSpeedConstants.AXE_SPEAR_SPEED;
        if (unit.equals(UnitSpeedConstants.LIGHT)) return UnitSpeedConstants.LIGHT_SPEED;
        if (unit.equals(UnitSpeedConstants.HEAVY)) return UnitSpeedConstants.HEAVY_SPEED;
        if (unit.equals(UnitSpeedConstants.SPY)) return UnitSpeedConstants.SPY_SPEED;
        return 0;
    }

    /**
     * Sucht anhand von Anonymen nach einer Einheit im gelieferten Text.
     *
     * @param text Text in dem nach einer Einheit gesucht werden soll.
     * @return korrekter Einheitennamen
     */
    public static String getUnitFromText(String text) {
        text = text.toUpperCase();
        if (text.contains("AG") || text.contains("ADELSGESCHLECHT") || text.contains("ADELGESCHLECHT")) {
            return UnitSpeedConstants.SNOB;
        } else if (text.contains("RAM") || text.contains("KATA")) {
            return UnitSpeedConstants.RAM_CATA;
        } else if (text.contains("SCHWÄRT") || text.contains("SCHWERT")) {
            return UnitSpeedConstants.SWORD;
        } else if (text.contains("AXT") || text.contains("ÄXTE") || text.contains("SPEER")) {
            return UnitSpeedConstants.AXE_SPEAR;
        } else if (text.contains("LKAV") || text.contains("LEICHTE")) {
            return UnitSpeedConstants.LIGHT;
        } else if (text.contains("SKAV") || text.contains("SCHWERE")) {
            return UnitSpeedConstants.HEAVY;
        } else if (text.contains("SPÄHER") || text.contains("SPY")) {
            return UnitSpeedConstants.SPY;
        }

        return "";
    }
}

package ch.omni.omnitool.home;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;

import ch.omni.omnitool.R;

public class HomeActivity extends Activity {
    private SharedPreferences sharedPref;
    private Boolean notificationAcces = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home2);
        sharedPref = getSharedPreferences(getString(R.string.sharedPrefs), Context.MODE_PRIVATE);

        Boolean registriert = sharedPref.getBoolean(getString(R.string.registriert), false);
        if (registriert) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new HomeFragment())
                    .commit();
        } else {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new NicknameFragment())
                    .commit();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!notificationAcces) {
            //Herausfinden ob der Benutzer die Einstellungen schon verändert hat.
            checkNotificationAccess();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_change_nick) {
            nextFragment(new NicknameFragment());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void nextFragment(Fragment fragment) {
        // Create new fragment and transaction
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    }

    /**
     * Prüft ob die App schon berechtigt ist Notifications abzufangen. Wenn nicht, erscheint eine AlertView
     * Ueber diese gelangt man direkt ins Menü und kann die Option setzen. Wenn man zurückkommt, geht es weiter
     * in der App.
     */
    private void checkNotificationAccess() {
        ContentResolver contentResolver = getContentResolver();
        String enabledNotificationListeners = Settings.Secure.getString(contentResolver, "enabled_notification_listeners");
        String packageName = getPackageName();

        // check to see if the enabledNotificationListeners String contains our package name
        if (enabledNotificationListeners == null || !enabledNotificationListeners.contains(packageName)) {
            // in this situation we know that the user has not granted the app the Notification access permission
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.einstellungen_nicht_korrekt)
                    .setPositiveButton(R.string.aktivieren, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            final Intent intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
                            startActivity(intent);
                            dialog.cancel();
                        }
                    })
                    .setNegativeButton(R.string.fertig, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            checkNotificationAccess();
                        }
                    });
            builder.create().show();
        } else {
            notificationAcces = true;
        }
    }

    public SharedPreferences getSharedPref() {
        return sharedPref;
    }
}

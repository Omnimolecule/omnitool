package ch.omni.omnitool.home;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import ch.omni.omnitool.R;
import ch.omni.omnitool.pojo.AidRequestAttackListElement;

public class AngriffsParserActivity extends Activity {

    private AidRequestAttackListElement aidRequestAttackListElement;
    private boolean useAttackText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_angriffs_parser);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new AidRequestFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.angriffs_parser, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void nextFragment() {
        // Create new fragment and transaction
        Fragment newFragment = new ResultFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack
        transaction.replace(R.id.container, newFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    }

    public AidRequestAttackListElement getAidRequestAttackListElement() {
        return aidRequestAttackListElement;
    }

    public void setAidRequestAttackListElement(AidRequestAttackListElement aidRequestAttackListElement) {
        this.aidRequestAttackListElement = aidRequestAttackListElement;
    }

    public boolean isUseAttackText() {
        return useAttackText;
    }

    public void setUseAttackText(boolean useAttackText) {
        this.useAttackText = useAttackText;
    }
}

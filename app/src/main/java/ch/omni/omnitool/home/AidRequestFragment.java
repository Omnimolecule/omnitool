package ch.omni.omnitool.home;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import ch.omni.omnitool.R;
import ch.omni.omnitool.doer.AidRequestParser;
import ch.omni.omnitool.pojo.AidRequestAttackListElement;
import ch.omni.omnitool.util.OmniException;

public class AidRequestFragment extends Fragment implements View.OnClickListener {

    public AidRequestFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.angriffsparser_aidrequest, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Button button = (Button) getView().findViewById(R.id.btnAidRequest);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnAidRequest) {
            EditText editText = (EditText) getView().findViewById(R.id.editAidRequest);
            String text = editText.getText().toString();
            try {
                AidRequestAttackListElement element = AidRequestParser.parseList(text);
                AngriffsParserActivity activity = (AngriffsParserActivity) getActivity();
                activity.setAidRequestAttackListElement(element);

                CheckBox checkBox = (CheckBox) getView().findViewById(R.id.chkBeschriftung);
                if (checkBox.isChecked()) {
                    activity.setUseAttackText(true);
                } else {
                    activity.setUseAttackText(false);
                }

            } catch (OmniException o) {
                //doSth with this Exception
            }
            ((AngriffsParserActivity) getActivity()).nextFragment();
        }
    }
}

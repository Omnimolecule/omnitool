package ch.omni.omnitool.home;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import ch.omni.omnitool.R;
import ch.omni.omnitool.database.AttackList;
import ch.omni.omnitool.database.DbHelper;

public class HomeFragment extends Fragment implements View.OnClickListener {
    private TextView lblNotificationList;

    private SQLiteDatabase db;
    private HomeActivity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (HomeActivity) getActivity();
        DbHelper adbHelper = new DbHelper(activity);
        db = adbHelper.getWritableDatabase();

        //Normale Gui Elemente
        TextView lblNickname = (TextView) getView().findViewById(R.id.lblNickname);
        Button btnAttackList = (Button) getView().findViewById(R.id.btnAttackList);
        Button btnDbLoeschen = (Button) getView().findViewById(R.id.btnDbLoeschen);
        lblNotificationList = (TextView) getView().findViewById(R.id.lblNotificationList);

        String savedNick = activity.getSharedPref().getString(getString(R.string.nickname), "");

        lblNickname.setText("Hallo " + savedNick);
        btnAttackList.setOnClickListener(this);
        btnDbLoeschen.setOnClickListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        reloadData(false);
    }


    private void reloadData(boolean delete) {
        //Hole die Einträge aus der DB und zeige sie an. Nur zu testzwecken ob es funktioniert.
        //Wird später wieder ausgebaut
        String[] projection = {AttackList.AttackEntry.COLUMN_NAME_TARGET_NAME, AttackList.AttackEntry.COLUMN_NAME_SEND_TIME};

        Cursor c = db.query(AttackList.AttackEntry.TABLE_NAME, projection, null, null, null, null, null);
        String ergebnis = "";

        if (c.getCount() > 0){
            c.moveToFirst();
            String a = c.getString(c.getColumnIndex(AttackList.AttackEntry.COLUMN_NAME_TARGET_NAME));
            String b = c.getString(c.getColumnIndex(AttackList.AttackEntry.COLUMN_NAME_SEND_TIME));
            DateFormat dtfm = new SimpleDateFormat("dd.MM.yy hh:mm:ss:SSS");
            ergebnis = "Ziel: " + a + " Zeit: " + dtfm.format(new Date(Long.valueOf(b))) + "      " + b + "\n";


            while (c.moveToNext()){
                a = c.getString(c.getColumnIndex(AttackList.AttackEntry.COLUMN_NAME_TARGET_NAME));
                b = c.getString(c.getColumnIndex(AttackList.AttackEntry.COLUMN_NAME_SEND_TIME));


                ergebnis += "Ziel: " + a + " Zeit: " + dtfm.format(new Date(Long.valueOf(b))) + "      " + b + "\n";
            }
        }
        if (!ergebnis.equals("")) {
            lblNotificationList.setText(ergebnis);
        } else if (delete) {
            lblNotificationList.setText(getString(R.string.lblNotificationList));
        }
        //-------------------------------------------------------------------
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnAttackList) {
            Intent intent = new Intent(activity, AngriffsParserActivity.class);
            startActivity(intent);

        } else if (view.getId() == R.id.btnDbLoeschen) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(R.string.datenbank_loeschen_alert)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            deleteOldDbEntries();
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            builder.create().show();
        }
    }

    /**
     * Löscht alle Einträge aus der AttackList Tabelle
     */
    private void deleteOldDbEntries() {
        String statement = "DELETE FROM " + AttackList.AttackEntry.TABLE_NAME;
        db.execSQL(statement);
        reloadData(true);
    }
}

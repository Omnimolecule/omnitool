package ch.omni.omnitool.home;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import ch.omni.omnitool.R;
import ch.omni.omnitool.doer.MasterPussyPimp;
import ch.omni.omnitool.pojo.AidRequestAttackListElement;
import ch.omni.omnitool.pojo.Attack;
import ch.omni.omnitool.pojo.Village;
import ch.omni.omnitool.util.OmniException;

public class ResultFragment extends Fragment {

    private HashMap<Village, ArrayList<Attack>> result;

    public ResultFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.angriffparser_result, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        AngriffsParserActivity activity = (AngriffsParserActivity) getActivity();
        AidRequestAttackListElement element = activity.getAidRequestAttackListElement();
        MasterPussyPimp mpp = new MasterPussyPimp();
        try {
            String ergebnis = "";
            result = mpp.compare(element, activity.isUseAttackText(), activity);
            for (Village village : result.keySet()) {
                ergebnis += "\nZieldorf: " + village.getCoordX() + "|" + village.getCoordY();
                ergebnis += "\n";
                ArrayList<Attack> atts = result.get(village);
                ergebnis += "Angriffe:\n";
                for (Attack att : atts) {
                    ergebnis += att.getUnit() + " - " + att.getSendVillage().getCoord() + " - " + new Date(att.getArrivalInMs());
                    ergebnis += "\n";
                }
            }
            TextView textView = (TextView) getView().findViewById(R.id.txtParserResult);
            textView.setText(ergebnis);

        } catch (OmniException e) {
            e.printStackTrace();
        }

        //Do the work here!
    }
}

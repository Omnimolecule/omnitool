package ch.omni.omnitool.home;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import ch.omni.omnitool.R;


public class NicknameFragment extends Fragment implements View.OnClickListener {
    private EditText txtNickname;


    public NicknameFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_nickname, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Gui Elemente um sich zu "registrieren"
        txtNickname = (EditText) getView().findViewById(R.id.txtNickname);
        Button btnNickname = (Button) getView().findViewById(R.id.btnSaveNickname);

        btnNickname.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnSaveNickname) {
            if (!txtNickname.getText().toString().equals("")) {
                SharedPreferences.Editor editor = getActivity().getSharedPreferences(getString(R.string.sharedPrefs), Context.MODE_PRIVATE).edit();
                editor.putString(getString(R.string.nickname), txtNickname.getText().toString());
                editor.putBoolean(getString(R.string.registriert), true);
                editor.apply();

                ((HomeActivity) getActivity()).nextFragment(new HomeFragment());


            }
        }
    }
}

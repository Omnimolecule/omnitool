package ch.omni.omnitool.database;


import android.provider.BaseColumns;

public class VillageList {

    public VillageList() {
    }

    public static final String SQL_CREATE_VILLAGELIST_ENTRIES =
            "CREATE TABLE " + VillageEntry.TABLE_NAME + " (" +
                    VillageEntry._ID + " INTEGER PRIMARY KEY ," +
                    VillageEntry.COLUMN_NAME_VILLAGE_NAME + DbHelper.TEXT_TYPE + DbHelper.COMMA_SEP +
                    VillageEntry.COLUMN_NAME_COORD_X + DbHelper.INTEGER_TYPE + DbHelper.COMMA_SEP +
                    VillageEntry.COLUMN_NAME_COORD_Y + DbHelper.INTEGER_TYPE + " )";


    public static final String SQL_DELETE_VILLAGELIST_TABLE =
            "DROP TABLE IF EXISTS " + VillageEntry.TABLE_NAME;

    public static abstract class VillageEntry implements BaseColumns {
        public static final String TABLE_NAME = "tbl_VillageList";
        public static final String COLUMN_NAME_VILLAGE_NAME = "village_name";
        public static final String COLUMN_NAME_COORD_X = "coord_x";
        public static final String COLUMN_NAME_COORD_Y = "coord_y";
    }
}

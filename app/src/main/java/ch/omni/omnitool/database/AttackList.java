package ch.omni.omnitool.database;

import android.provider.BaseColumns;

public class AttackList {

    public AttackList(){}

    public static final String SQL_CREATE_ATTACKLIST_ENTRIES =
            "CREATE TABLE " + AttackEntry.TABLE_NAME + " (" +
                    AttackEntry._ID + " INTEGER PRIMARY KEY ," +
                    AttackEntry.COLUMN_NAME_TARGET_NAME + DbHelper.TEXT_TYPE + DbHelper.COMMA_SEP +
                    AttackEntry.COLUMN_NAME_SEND_TIME + DbHelper.DATE_TYPE + " )";

    public static final String SQL_DELETE_ATTACKLIST_TABLE =
            "DROP TABLE IF EXISTS " + AttackEntry.TABLE_NAME;

    public static abstract class AttackEntry implements BaseColumns {
        public static final String TABLE_NAME = "tbl_AttackList";
        public static final String COLUMN_NAME_TARGET_NAME = "target_name";
        public static final String COLUMN_NAME_SEND_TIME= "send_time";
    }


}